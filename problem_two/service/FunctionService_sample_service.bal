import ballerina/grpc;
import ballerina/io;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "FunctionService" on ep {

    remote function add_fns(Function value) returns FunctionResponse|error {
        // Implementation goes here.
     

    }
    remote function delete_fn(string value) returns FunctionResponse|error {
        // Implementation goes here.
 // You should return a FunctionResponse
    }

    remote function show_fn(ShowFunctionRequest value) returns Function|error {
        // Implementation goes here.
  // You should return a Function
    }

    remote function add_new_fn(stream<Function, grpc:Error?> clientStream) returns FunctionResponse|error {
        // Implementation goes here.

    // You should return a FunctionResponse

    }

    remote function show_all_fns(string value) returns stream<Function, error?>|error {
        // Implementation goes here.

    }

    remote function show_all_with_criteria(stream<string, grpc:Error?> clientStream) returns stream<Function, error?>|error {
        // Implementation goes here.
    }
}

