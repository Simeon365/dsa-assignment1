import ballerina/grpc;

public isolated client class FunctionServiceClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_fns(Function|ContextFunction req) returns (FunctionResponse|grpc:Error) {
        map<string|string[]> headers = {};
        Function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionService/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <FunctionResponse>result;
    }

    isolated remote function add_fnsContext(Function|ContextFunction req) returns (ContextFunctionResponse|grpc:Error) {
        map<string|string[]> headers = {};
        Function message;
        if (req is ContextFunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionService/add_fns", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <FunctionResponse>result, headers: respHeaders};
    }

    isolated remote function delete_fn(string|ContextString req) returns (FunctionResponse|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionService/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <FunctionResponse>result;
    }

    isolated remote function delete_fnContext(string|ContextString req) returns (ContextFunctionResponse|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionService/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <FunctionResponse>result, headers: respHeaders};
    }

    isolated remote function show_fn(ShowFunctionRequest|ContextShowFunctionRequest req) returns (Function|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFunctionRequest message;
        if (req is ContextShowFunctionRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionService/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <Function>result;
    }

    isolated remote function show_fnContext(ShowFunctionRequest|ContextShowFunctionRequest req) returns (ContextFunction|grpc:Error) {
        map<string|string[]> headers = {};
        ShowFunctionRequest message;
        if (req is ContextShowFunctionRequest) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("FunctionService/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <Function>result, headers: respHeaders};
    }

    isolated remote function add_new_fn() returns (Add_new_fnStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("FunctionService/add_new_fn");
        return new Add_new_fnStreamingClient(sClient);
    }

    isolated remote function show_all_fns(string|ContextString req) returns stream<Function, grpc:Error?>|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("FunctionService/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        FunctionStream outputStream = new FunctionStream(result);
        return new stream<Function, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext(string|ContextString req) returns ContextFunctionStream|grpc:Error {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeServerStreaming("FunctionService/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        FunctionStream outputStream = new FunctionStream(result);
        return {content: new stream<Function, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("FunctionService/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_new_fnStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendFunction(Function message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextFunction(ContextFunction message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveFunctionResponse() returns FunctionResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <FunctionResponse>payload;
        }
    }

    isolated remote function receiveContextFunctionResponse() returns ContextFunctionResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <FunctionResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class FunctionStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|Function value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|Function value;|} nextRecord = {value: <Function>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendString(string message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextString(ContextString message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveFunction() returns Function|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <Function>payload;
        }
    }

    isolated remote function receiveContextFunction() returns ContextFunction|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <Function>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class FunctionServiceFunctionResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunctionResponse(FunctionResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunctionResponse(ContextFunctionResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionServiceFunctionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendFunction(Function response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextFunction(ContextFunction response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextFunctionStream record {|
    stream<Function, error?> content;
    map<string|string[]> headers;
|};

public type ContextStringStream record {|
    stream<string, error?> content;
    map<string|string[]> headers;
|};

public type ContextFunction record {|
    Function content;
    map<string|string[]> headers;
|};

public type ContextShowFunctionRequest record {|
    ShowFunctionRequest content;
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextFunctionResponse record {|
    FunctionResponse content;
    map<string|string[]> headers;
|};

public type Function record {|
    string key = "";
    string language = "";
    string functionDescription = "";
    string 'version = "";
|};

public enum Developer {
    name,
    email
}

public type ShowFunctionRequest record {|
    string key = "";
    int 'version = 0;
|};

public type FunctionResponse record {|
    string key = "";
|};

const string ROOT_DESCRIPTOR = "0A1470726F746F636F6C4275666665722E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22A6010A0846756E6374696F6E12100A036B657918012001280952036B6579121A0A086C616E677561676518022001280952086C616E677561676512300A1366756E6374696F6E4465736372697074696F6E180320012809521366756E6374696F6E4465736372697074696F6E12180A0776657273696F6E180420012809520776657273696F6E22200A09446576656C6F70657212080A046E616D65100012090A05656D61696C100122240A1046756E6374696F6E526573706F6E736512100A036B657918012001280952036B657922410A1353686F7746756E6374696F6E5265717565737412100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E32D4020A0F46756E6374696F6E5365727669636512270A076164645F666E7312092E46756E6374696F6E1A112E46756E6374696F6E526573706F6E7365122C0A0A6164645F6E65775F666E12092E46756E6374696F6E1A112E46756E6374696F6E526573706F6E73652801123C0A0964656C6574655F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A112E46756E6374696F6E526573706F6E7365122A0A0773686F775F666E12142E53686F7746756E6374696F6E526571756573741A092E46756E6374696F6E12390A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46756E6374696F6E300112450A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46756E6374696F6E28013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "protocolBuffer.proto": "0A1470726F746F636F6C4275666665722E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22A6010A0846756E6374696F6E12100A036B657918012001280952036B6579121A0A086C616E677561676518022001280952086C616E677561676512300A1366756E6374696F6E4465736372697074696F6E180320012809521366756E6374696F6E4465736372697074696F6E12180A0776657273696F6E180420012809520776657273696F6E22200A09446576656C6F70657212080A046E616D65100012090A05656D61696C100122240A1046756E6374696F6E526573706F6E736512100A036B657918012001280952036B657922410A1353686F7746756E6374696F6E5265717565737412100A036B657918012001280952036B657912180A0776657273696F6E180220012805520776657273696F6E32D4020A0F46756E6374696F6E5365727669636512270A076164645F666E7312092E46756E6374696F6E1A112E46756E6374696F6E526573706F6E7365122C0A0A6164645F6E65775F666E12092E46756E6374696F6E1A112E46756E6374696F6E526573706F6E73652801123C0A0964656C6574655F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A112E46756E6374696F6E526573706F6E7365122A0A0773686F775F666E12142E53686F7746756E6374696F6E526571756573741A092E46756E6374696F6E12390A0C73686F775F616C6C5F666E73121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46756E6374696F6E300112450A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A092E46756E6374696F6E28013001620670726F746F33"};
}

