import ballerina/io;
import ballerina/grpc;

FunctionServiceClient ep = check new ("http://localhost:9090");

public function main() {
}

public function add_fns() returns error? {
    Function[] FunctionArray = [];
    //adding multiple functions 

    Function function_1 = {
        key: "1",
        Developer: {
            name: "Nestor",
            email: "nessy@yahoo.com"
        },
        language: "JavaScript",
        functionDescription: ["First description", "Second description", "Third description"]
    };
    Function function_2 = {
        key: "2",
        Developer: {
            name: "Simeon",
            email: "simeon@yahoo.com"
        },
        language: "Python",
        functionDescription: ["First description", "Second description", "Third description"]
    };
    //add Functions to array
    FunctionArray.push(function_1);
    FunctionArray.push(function_2);
}

public function add_new_fn() {

    Finction Function_1 = {
        key: "3",
        Developer: {
            name: "Nestor",
            email: "nessy@yahoo.com"
        },
        language: "JavaScript",
        functionDescription: ["First description", "Second description", "Third description"]
    };
    //adding new function

    var response = ep->add_new_fn(Function_1);
    if response is grpc:Error {
        io:println(response.toString());
    } else {
        io:println(response);
    }

}
public function show_all_fns() returns error? {}

public function delete_fn() {}

public function show_fn() {
    ShowFunctionRequest Req_id = {key:"Function_1" , "version":0};
    var response =  ep->show_fn(Req_id);


    if response is grpc:Error {
        io:println(response.toString());
    } else {
        io:println(response);
    }

}
public function show_all_with_criteria() returns error?{}