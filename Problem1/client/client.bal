import ballerina/io;
import ballerina/http;

final http:Client clientEndpoint = check new ("http://localhost:8080");

public function main() returns error? {
    json studentprofile = {
                username: "Alien", 
                lastname: "Hifikepunye", 
                firstname: "Simeon", 
                preferred_formats: ["audio", "video", "text"], 
                past_subjects: [
                        {
                        course: "DPG", 
                        score: "B+"
                        }, 
                        {
                        course: "Advanced Programming", 
                        score: "A+"
                        }
                    ], 
};

    json learning_materials = {
            course: "Distributed Systems Applications", 
            learning_objects: {
            required: {
            audio: [
                {
                    name: "Topic 1", 
                    description: "", 
                    difficulty: ""
                }
            ], 
            text: [
                {

                }
            ]
            }, 
            suggested: {
                    video: [], 
                    audio: []
                }
            }
    };

    //string value = check clientEndpoint->get("/hello/Update/'dd'/22");

    json updatedstudentprofile = {
                username: "Simeon365", 
                preferred_formats: ["audio", "video", "text"], 
                past_subjects: [
                        {
                        course: "Data Structures & Algorithms", 
                        score: "C+"
                        }, 
                        {
                        course: "MIT", 
                        score: "A+"
                        }, 
                        {
                        course: "Programming 2", 
                        score: "A+"
                        }, 
                        {
                        course: "EAP", 
                        score: "B+"
                        }, 
                        {
                        course: "Database Fundementals", 
                        score: "C+"
                        }
                    ]
                };

    io:println("------------Assignment Repo-------------");
    io:println("1. Create Profile");
    io:println("2. Update Profile");
    io:println("3. Create Learning Material ");
    io:println("----------------------------------------");
    string choose = io:readln("Please Enter an  Option: ");
    if (choose === "1") {
        json value = check clientEndpoint->post("/createStudentProfile", studentprofile);
        io:println(value);
    } else if (choose === "2") {
        json updatedvalue = check clientEndpoint->post("/updateStudentProfile", updatedstudentprofile);
        io:println(updatedvalue);
    } else if (choose === "3") {
        json value = check clientEndpoint->post("/createLearningMaterial", learning_materials);
    }
}
