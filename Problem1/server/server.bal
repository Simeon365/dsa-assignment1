import ballerina/io;
import ballerina/http;
import ballerinax/mongodb;

mongodb:ClientConfig mongoConfig = {
    host: "localhost",
    port: 9090,
    username: "function",
    password: "pass123",
    options: {sslEnabled: false, serverSelectionTimeout: 5000}
};
mongodb:Client mongoClient = check new (mongoConfig, "DSPAssign");

service / on new http:Listener(8080) {
    resource function post createStudentProfile(@http:Payload json learnerprofile) returns json|error? {
        io:println(learnerprofile.toJsonString());
        map<json> studentprofile = <map<json>>learnerprofile;
        checkpanic mongoClient->insert(studentprofile,"learner_profile");
        return learnerprofile;
    }

    resource function post updateStudentProfile(@http:Payload json newLprofile) returns json|error?{
        string message="";
        map<json> updatedprofile = <map<json>>newLprofile;
        map<json>|error updatefilter = {"username":check updatedprofile.username};
        if(updatefilter is error){
            io:println("Error");
        }else{
            int response = checkpanic mongoClient->update(updatedprofile, "learner_profile", (), updatefilter , true);
            if (response > 0 ) {
                io:println("Modified count: '" + response.toString() + "'.") ;
                message = "succesfully Updated!";
            } else {
                io:println("Nothing Was Updated.");
                message = "Nothing Was Updated!";
            }
        }
    

        json response = {message:message};
        return response;
    }


    resource function post createLearningMaterial(@http:Payload json learnerprofile) returns json|error? {
        io:println(learnerprofile.toJsonString());
        map<json> learningMaterial = <map<json>>learnerprofile;
        checkpanic mongoClient->insert(learningMaterial,"learning_material");
        return learnerprofile;
    }
    resource function post getStudentMaterials(@http:Payload json learnerprofile) returns json {
        json alltopics = {};

        return alltopics;
    }
}